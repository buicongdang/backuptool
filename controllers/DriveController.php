<?php
namespace app\controllers;

use yii\web\Controller;

class DriveController extends Controller
{
    public function actionIndex()
    {
        echo '<pre>';
        var_dump($this->authGoogle());
        echo '</pre>';
        die;
    }

    public function authGoogle()
    {
        $client_email = 'backupwsddaily@websongdep-6789.iam.gserviceaccount.com';
        $private_key = file_get_contents('key/WebSongDep-c8341ba08020.p12');
        $scopes = 'https://www.googleapis.com/auth/drive';


        $credentials = new \Google_Auth_AssertionCredentials($client_email,$scopes,$private_key);

        $client = new \Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion();
        }

        $service = new \Google_Service_Drive($client);
        if ($client->getAccessToken()) {
            $file = new \Google_Service_Drive_DriveFile();
            $file->setTitle("123.png");
            $result = $service->files->insert(
                $file,
                array(
                    'data' => file_get_contents('key/1.png'),
                    'mimeType' => 'application/octet-stream',
                    'uploadType' => 'media'
                )
            );
        }

        return $result;
    }
}