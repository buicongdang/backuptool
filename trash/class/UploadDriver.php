<?php
session_start();
ob_start();


class UploadDriver
{
    public function upload($file_source,$code)
    {
        $client = new Google_Client();
        $client->setAuthConfigFile('client_secret.json');

        $client->addScope("https://www.googleapis.com/auth/drive");
        $service = new Google_Service_Drive($client);

        if (isset($_REQUEST['logout'])) {
            unset($_SESSION['upload_token']);
        }
        if (isset($code)) {
            $client->authenticate($code);
            $_SESSION['upload_token'] = $client->getAccessToken();
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
        }

        if (isset($_SESSION['upload_token']) && $_SESSION['upload_token']) {
            $client->setAccessToken($_SESSION['upload_token']);
            if ($client->isAccessTokenExpired()) {
                unset($_SESSION['upload_token']);
            }
        } else {
            $authUrl = $client->createAuthUrl();
        }

        if ($client->getAccessToken())
        {
            // Now lets try and send the metadata as well using multipart!
            $file = new Google_Service_Drive_DriveFile();
            $file->setTitle('Backup_WSD_'.date('Y_m_d_h_i',time()).'.zip');
            $result = $service->files->insert(
                $file,
                array(
                    'data' => file_get_contents($file_source),
                    'mimeType' => 'application/octet-stream',
                    'uploadType' => 'multipart'
                )
            );
        }

        if(isset($authUrl))
        {
            ?>

<?php
            header('Location: http://backuptool.com/index2.php');
//            header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
        }
    }



    public function upload_single($file_source,$code)
    {
        $client = new Google_Client();
        $client->setAuthConfigFile('client_secret.json');

        $client->addScope("https://www.googleapis.com/auth/drive");
        $auth_url = $client->createAuthUrl();

        if(isset($auth_url))
        {
            header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        }

        $client->authenticate($code);
        $access_token = $client->getAccessToken();

        $client->setAccessToken($access_token);

        $service = new Google_Service_Drive($client);


        if ($client->getAccessToken()) {
            // Now lets try and send the metadata as well using multipart!

            $file = new Google_Service_Drive_DriveFile();
            $file->setTitle('Backup_WSD_'.date('Y_m_d_h_i',time()).'.zip');
//            $file->setTitle('Backup_WSD_'.date('Y_m_d_h_i',time()).'.zip');
            $result = $service->files->insert(
                $file,
                array(
                    'data' => file_get_contents($file_source),
                    'mimeType' => 'application/octet-stream',
                    'uploadType' => 'multipart'
                )
            );
            return $result;
        }

        return false;
    }

}
?>