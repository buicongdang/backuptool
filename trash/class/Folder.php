<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 1/18/16
 * Time: 11:33 PM
 */
class Folder
{
    public function zip_folder($folder_source = '')
    {
        $arr_files = $this->arr_file_folder($folder_source);
        $file_name = 'Backup_WSD_'.date('Y_m_d_h_i',time()).'.zip';
        $zip = new ZipArchive();
        if ($zip->open($file_name,ZipArchive::CREATE | ZipArchive::OVERWRITE ) != true)
        {
            die('Cannot open file '.$file_name);
        }
        foreach($arr_files as $k => $v)
        {
            $zip->addFile($v);
        }
        $zip->close();

        return $file_name;
    }

    public function strip_file($array = array(), $files = array('.','..','.DS_Store'))
    {
        if(empty($files))
        {
            return false;
        }
        return array_diff($array,$files);
    }

    public function arr_file_folder($folder_source = '')
    {
        $arr_file = array();
        if( ! is_dir($folder_source))
        {
            return false;
        }

        $list_file = $this->strip_file(scandir($folder_source));
        foreach($list_file as $k => $v)
        {
            if(is_file($folder_source.'/'.$v))
            {
                $arr_file[] = $folder_source.'/'.$v;
            }
            if(is_dir($folder_source.'/'.$v))
            {
                $arr_file = array_merge($arr_file,$this->arr_file_folder($folder_source.'/'.$v));
            }

        }
        return $arr_file;
    }

}


