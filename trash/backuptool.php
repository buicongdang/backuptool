<?php
/**
 * Created by PhpStorm.
 * User: buicongdang
 * Date: 1/18/16
 * Time: 11:13 PM
 */
/* creates a compressed zip file */
function create_zip($files = array(),$destination = '',$overwrite = true) {
    //if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) {
        return false;
    }

    $valid_files = array();
    //if files were passed in...
    if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
            //make sure the file exists
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach($valid_files as $file) {
            $zip->addFile($file);
        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

        //close the zip -- done!
        $zip->close();

        //check to make sure the file exists
        return file_exists($destination);
    }
    else
    {
        return false;
    }
}


$dir = '123/';
$listfiles = scandir($dir);
unset($listfiles[0]);
unset($listfiles[1]);
var_dump($listfiles);die;
//
//
//$listfiles = array(
//    '123/1.pptx',
//    '123/2.xlsx',
//);

//if true, good; if false, zip creation failed
$result = create_zip($listfiles,'my-archive.zip');

var_dump($result);
die;